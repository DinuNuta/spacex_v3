//
//  LaunchList_Cell.swift
//  SpaceX
//
//  Created by Coscodan Dinu on 07/02/2020.
//  Copyright © 2020 Dinu_c All rights reserved.
//

import UIKit
import Kingfisher

struct LaunchCellViewModele {
    let title: String
    let formatedDate: String?
    let thumbnailURL: URL?
}

extension LaunchList_Cell: NibLoadableView {}

class LaunchList_Cell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 15
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowRadius = 10
        layer.shadowOpacity = 0.5
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.thumbnailImageView.kf.cancelDownloadTask()
        self.thumbnailImageView.image = nil

        self.titleLabel.text = nil
        self.dateLabel.text = nil

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func config(with viewModel: LaunchCellViewModele) {
        DispatchQueue.main.async {
            self.titleLabel.text = viewModel.title
            self.dateLabel.text = viewModel.formatedDate
        }

        guard let imgSource = viewModel.thumbnailURL else {
            DispatchQueue.main.async {
                self.thumbnailImageView.image = UIImage(named: "img-placeholder")
            }
            return
        }
        thumbnailImageView.kf.setImage(with: imgSource,
                                       placeholder: UIImage(named: "img-placeholder"))

    }
}
