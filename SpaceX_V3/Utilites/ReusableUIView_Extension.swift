//
//  ReusableUIView_Extension.swift
//
//  Created by Coscodan Dinu on 6/7/17.
//  Copyright © 2017 Coscodan.Dinu. All rights reserved.
//
import Foundation
import UIKit
// MARK: - CollectionView / UItableRegister ReusableView for cell

extension UICollectionViewCell: ReusableView { }

extension UITableViewCell: ReusableView { }
extension UITableViewHeaderFooterView: ReusableView { }

public protocol ReusableView: class { }

public extension ReusableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

public protocol NibLoadableView: class { }

public extension NibLoadableView where Self: UIView {
    static var NibName: String {
        return String(describing: self)
    }
    static var bundle: Bundle? {
        return Bundle(for: self)
    }
}

public extension NibLoadableView where Self: UIViewController {
    static var NibName: String {
        return String(describing: self)
    }
    static var bundle: Bundle? {
        return Bundle(for: self)
    }
}

public extension UITableView {

    func scrollToBottom() {

        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.numberOfRows(inSection: self.numberOfSections - 1) - 1,
                section: self.numberOfSections - 1)
            self.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }

    func scrollToTop() {
        if self.numberOfRows(inSection: 0) > 0 {
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: 0, section: 0)
                self.scrollToRow(at: indexPath, at: .top, animated: false)
            }
        } else {
            let rect = CGRect.init(x: 0, y: 0,
                                   width: self.frame.width,
                                   height: self.frame.height)
            DispatchQueue.main.async {
                self.scrollRectToVisible(rect, animated: true)
            }
        }
    }
}

// MARK: - UICollectionView register cell ReusableView
public extension UICollectionView {
    func register<T: UICollectionViewCell>(_: T.Type) where T: NibLoadableView {
        let nib = UINib(nibName: T.NibName, bundle: T.bundle)
        register(nib, forCellWithReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
}

// MARK: - UICollectionView register HeaderFooterView OfKind
public extension UICollectionView {
    func register<T: UICollectionReusableView>(_: T.Type, forSupplementaryViewOfKind kind: String) where T: ReusableView, T: NibLoadableView {
        let nib = UINib(nibName: T.NibName, bundle: T.bundle)
        register(nib, forSupplementaryViewOfKind: kind, withReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableSupplementaryView<T: UICollectionReusableView>(OfKind kind: String, forIndexPath indexPath: IndexPath) -> T where T: ReusableView {
        guard let view = dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue view with identifier: \(T.reuseIdentifier)")
        }
        return view
    }
}

// MARK: - UITableView register cell ReusableView
public extension UITableView {

    func register<T: UITableViewCell>(_: T.Type) where T: NibLoadableView {
        let nib = UINib(nibName: T.NibName, bundle: T.bundle)
        register(nib, forCellReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T
            else {
                fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }

}

// MARK: - UITableView register HeaderFooterView
public extension UITableView {
    func register<T: UITableViewHeaderFooterView>(_: T.Type) where T: NibLoadableView {
        let nib = UINib(nibName: T.NibName, bundle: T.bundle)
        register(nib, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusebleHeaderFooterView<T: UITableViewHeaderFooterView>() -> T {
        guard let view = dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as? T
            else {
                fatalError("Could not dequeue Header or Footer view with identifier: \(T.reuseIdentifier)")
        }
        return view
    }

}
