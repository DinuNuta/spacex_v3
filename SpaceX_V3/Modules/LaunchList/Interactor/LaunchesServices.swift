//
//  LaunchesServices.swift
//  SpaceX_V3
//
//  Created by Dinu_c on 2/7/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import Alamofire

enum Ordered: String {
    case desc
    case asc
}

protocol LaunchesServices: BaseServices {
    func getLaunches(by offset: Int, ordered: Ordered, completion: @escaping (Result<[Launch]>) -> Void)
}

struct LaunchesServicesImp: LaunchesServices {
    var requestManager: SessionManager = Alam_DefaultHeaders.sender

    func getLaunches(by offset: Int, ordered: Ordered, completion: @escaping (Result<[Launch]>) -> Void) {
        let params: [String: Any] = [NK.limit.rawValue: constants.limitResults,
                                     NK.offset.rawValue: offset,
                                     NK.order.rawValue: ordered.rawValue]
        request(RequestHttp(url: URLs.pastLaunches, method: .get, parameters: params), completion: completion)
    }
}
