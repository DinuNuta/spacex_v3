//
//  LaunchListRouter.swift
//  SpaceX_V3
//
//  Created by Coscodan Dinu on 07/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

protocol LaunchListRouter {
    func openLaunchDetails(with launch: Launch)
}

class LaunchListRouterImp: LaunchListRouter {

    weak var viewController: LaunchListViewController!

    func openLaunchDetails(with launch: Launch) {
        guard let detailsVC = LaunchDetailsDefaultBuilder().buildLaunchDetailsViewController(with: launch)
            else { print("Didn't init launch details module!"); return }

        guard let nav = viewController.navigationController
            else {
                viewController.present(detailsVC, animated: true, completion: nil)
                return }

        nav.pushViewController(detailsVC, animated: true)
    }

}
