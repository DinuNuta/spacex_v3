//
//  Launch.swift
//  SpaceX_V3
//
//  Created by Dinu_c on 2/7/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

struct Launch: Codable {
    let flightNumber: Int
    let missionName: String
    let missionId: [String]
    let upcoming: Bool
    let details: String?
    let launchDateUnix: Date?
    let links: Links
    let rocket: Rocket

    //    let rocketName: String
    var massKG: Float {
        return rocket.secondStage.payloads.first?.payloadMassKg ?? 0
    }

}
struct Links: Codable {
    let missionPatch, missionPatchSmall: String?
    let youtubeId: String?
    let articleLink, wikipedia, videoLink: String?
    let flickrImages: [String]
}

struct Rocket: JSONModel {
    let rocketId: String
    let rocketName: String
    let rocketType: String
    let secondStage: SecondStage
}

struct SecondStage: Codable {
    let block: Int?
    let payloads: [Payload]
    enum CodingKeys: String, CodingKey {
        case block
        case payloads
    }
}

struct Payload: Codable {
    let payloadId: String
    let noradId: [Int]
    let reused: Bool
    let customers: [String?]
    let nationality, manufacturer, payloadType: String?
    let payloadMassKg, payloadMassLbs: Float?
    let orbit: String?
}
