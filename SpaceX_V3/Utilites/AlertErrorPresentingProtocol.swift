//
//  AlertPresenterProtocol.swift
//
//  Created by Coscodan Dinu on 6/8/17.
//  Copyright © 2017 Coscodan.Dinu. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

public protocol AlertErrorPresenting: ActivitYPresentingProtocol {
    func showAlert(title: String, message: String)
    func showAlert(message: String, title: String?, ok: String, cancel: String, completion: (() -> Void)?)
    func showAlertError(error: Error)
    func showAlertError(alert: UIAlertController, animated: Bool, completion: (() -> Void)?)
    func didShowAlertError()
    var mainViewController: UIViewController? {get}
}

extension AlertErrorPresenting {
    func showAlertError(error: Error) {
        var title = "Error"
        var message = error.localizedDescription

        // MARK: Catch errore case
        switch error {
        case ErrorMessages.networkError( _, let titleLocal, let messageLocal):
            title = titleLocal
            message = messageLocal
        case is AFError :
            guard let alamError = error as? AFError else { break }
            title = "Error"
            message = alamError.errorDescription ??  alamError.localizedDescription
        case is DecodingError:
            guard let decodeError = error as? DecodingError else { break }
            message = decodeError.debugDescription
        default:
            message = error.localizedDescription
        }

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: {(_: UIAlertAction!) in}))

        DispatchQueue.main.async {
            self.showAlertError(alert: alert, animated: true, completion: nil)
        }
        didShowAlertError()
    }

    func showAlertLocalize(message: String, title: String?=nil, ok: String = "OK", cancel: String = "cancel", completion: (() -> Void)? ) {
        showAlert(message: message.localizedLowercase,
                  title: title?.localizedLowercase, ok: ok.localizedLowercase,
                  cancel: cancel.localizedLowercase, completion: completion)
    }

    func showAlert(message: String, title: String?=nil, ok: String = "OK", cancel: String = "cancel", completion: (() -> Void)? ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancel.localizedLowercase, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: ok, style: .default, handler: {(_: UIAlertAction!) in
            completion?()
        }))
        DispatchQueue.main.async {
            self.showAlertError(alert: alert, animated: true, completion: nil)
        }
    }

    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: {(_: UIAlertAction!) in}))
        DispatchQueue.main.async {
            self.showAlertError(alert: alert, animated: true, completion: nil)
        }
        didShowAlertError()
    }

    func didShowAlertError() {}
}

public extension AlertErrorPresenting where Self: UIViewController {

    func showAlertError(alert: UIAlertController, animated: Bool=true, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            self.present(alert, animated: animated, completion: completion)
        }
        didShowAlertError()
    }

    var mainViewController: UIViewController? {
        return self
    }
}

public extension AlertErrorPresenting {
    func showAlertError(alert: UIAlertController, animated: Bool=true, completion: (() -> Void)? = nil) {
        self.hideActivity()
        if let mainVC = UIApplication.topViewController() {
            DispatchQueue.main.async {
                mainVC.present(alert, animated: true, completion: nil)
            }
            didShowAlertError()
        }
    }

    var mainViewController: UIViewController? {
        return UIApplication.topViewController()
    }
}

extension DecodingError {

    public var debugDescription: String {
        switch  self {
        case .dataCorrupted(let context):
            return NSLocalizedString(context.debugDescription, comment: "")
        case .keyNotFound(let key, let context):
            return "\(key.stringValue) - Key not Found. \n \(context.debugDescription)"
        case .typeMismatch(let type, let context):
            return "\(type.self) - Type Mismatch. \n \(context.debugDescription)"
        case .valueNotFound(let type, let context):
            let codingPathStr = context.codingPath.map({$0.stringValue}).joined(separator: ".")
            return "\(codingPathStr) - \(type.self) - Value Not Found. \n \(context.debugDescription)"
        @unknown default:
            return NSLocalizedString("\(self.errorDescription ?? "Unknown Error")", comment: "")
        }
    }
}
