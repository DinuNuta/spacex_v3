//
//  LaunchListPresenter.swift
//  SpaceX_V3
//
//  Created by Coscodan Dinu on 07/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

protocol LaunchListPresenter {
    func pullToRefresh()
    func getLaunches()
    func item(at indexPath: IndexPath) -> LaunchCellViewModele
    func didSelectItem(at indexPath: IndexPath)
    func numberOfItems() -> Int
}

class LaunchListPresenterImp: LaunchListPresenter {

    weak var view: LaunchListView!
    var interactor: LaunchListInteractor!
    var router: LaunchListRouter!

    var launches: [Launch] = []

    init(view: LaunchListView, interactor: LaunchListInteractor, router: LaunchListRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }

}

extension LaunchListPresenterImp {
    func pullToRefresh() {
        interactor.getLaunches(by: 0) {[weak self] (result) in
            do {
                let launches = try result.get()
                self?.launches = launches
                self?.view.reloadTable()
            } catch {
                self?.view.didEndRefrehing()
                self?.view.showAlertError(error: error)
            }
        }
    }

    func getLaunches() {
        interactor.getLaunches(by: launches.count) {[weak self] (result) in
            guard let self = self else {return}
            do {
                let newLaunches = try result.get()
                let lastCount = self.launches.count
                self.launches.append(contentsOf: newLaunches)
                var indexPaths = [IndexPath]()
                for indx in lastCount ..< self.launches.count {
                    indexPaths.append(IndexPath(item: indx, section: 0))
                }
                self.view.didEndInfinitScroll(insertedIndx: indexPaths)
                if newLaunches.count < constants.limitResults {
                    self.view.noticeNoMoreData()
                }
            } catch {
                self.view.didEndInfinitScroll(insertedIndx: [])
                self.view.showAlertError(error: error)
            }
        }
    }

    func item(at indexPath: IndexPath) -> LaunchCellViewModele {
        let launch = launches[indexPath.row]
        let thumnails = launch.links.flickrImages.first
        let iconUrl = thumnails.flatMap({URL(string: $0)})

        return LaunchCellViewModele.init(title: launch.missionName,
                                         formatedDate: launch.launchDateUnix?.MMMMdYYYY,
                                         thumbnailURL: iconUrl)
    }

    func didSelectItem(at indexPath: IndexPath) {
        let launch = launches[indexPath.row]
        router.openLaunchDetails(with: launch)
    }

    func numberOfItems() -> Int {
        return launches.count
    }
}
