//
//  URLs.swift
//  SpaceX_V3
//
//  Created by Dinu_c on 2/6/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

public struct URLs {
    static var base = URL(string: "https://api.spacexdata.com")!
    static var api = base.appendingPathComponent("v3")
    static var launches = api.appendingPathComponent("launches")
    static var pastLaunches = launches.appendingPathComponent("past")
    static var getRocketsInfo = api.appendingPathComponent("rockets")

    static var baseYv3 = URL(string: "https://www.googleapis.com/youtube/v3/")!
    static var yVideos = baseYv3.appendingPathComponent("videos")
}

public extension URL {
    func appendingQuery(withParameters parameters: [URLQueryItem]) -> URL {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)!
        if components.queryItems != nil {
            components.queryItems?.append(contentsOf: parameters)
        } else {
            components.queryItems = parameters }
        return components.url!
    }

    var queryItems: [URLQueryItem] {
        let components = URLComponents(url: self, resolvingAgainstBaseURL: true)!
        return components.queryItems ?? []
    }

}
