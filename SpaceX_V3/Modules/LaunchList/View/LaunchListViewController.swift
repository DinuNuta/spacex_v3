//
//  LaunchListViewController.swift
//  SpaceX_V3
//
//  Created by Coscodan Dinu on 07/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import UIKit
import ESPullToRefresh

protocol LaunchListView: class, AlertErrorPresenting {
    func reloadTable()
    func didEndRefrehing()
    func didEndInfinitScroll(insertedIndx: [IndexPath])
    func noticeNoMoreData()
}

class LaunchListViewController: UIViewController, LaunchListView {

    var presenter: LaunchListPresenter!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: MYRefreshHeaderAnimator!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "SpaceX"
        self.navigationController?.navigationBar.tintColor = .blackPearl
        configTableView()
        tableView.es.startPullToRefresh()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func configTableView() {
        self.tableView.register(LaunchList_Cell.self)
        self.tableView.rowHeight = 240

        tableView.es.addPullToRefresh(animator: self.headerView) {[weak self] in
            self?.refreshData()
        }
        tableView.es.addInfiniteScrolling {[weak self] in
            self?.presenter.getLaunches()
        }
    }
}

extension LaunchListViewController {
    func reloadTable() {
        self.tableView.es.stopPullToRefresh()
        tableView.reloadData()
    }

    func didEndRefrehing() {
        self.tableView.es.stopPullToRefresh()
    }

    @objc func refreshData() {
        self.presenter.pullToRefresh()
    }

    func didEndInfinitScroll(insertedIndx: [IndexPath]) {
        if insertedIndx.isEmpty {
            noticeNoMoreData()
            return
        }
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: insertedIndx, with: .fade)
            self.tableView.endUpdates()
            self.tableView.es.stopLoadingMore()
        }
    }

    func noticeNoMoreData() {
        self.tableView.es.noticeNoMoreData()
    }

}

extension LaunchListViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfItems()
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cellLaunch = cell as? LaunchList_Cell  else { return }
        cellLaunch.thumbnailImageView.kf.cancelDownloadTask()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as LaunchList_Cell
        let viewModel = presenter.item(at: indexPath)
        cell.config(with: viewModel)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectItem(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
