//
//  HeaderAnimator.swift
//  SpaceX_V3
//
//  Created by Dinu_c on 2/12/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import QuartzCore
import UIKit
import ESPullToRefresh

extension MYRefreshHeaderAnimator: CustomView {}

open class MYRefreshHeaderAnimator: UIView, ESRefreshProtocol, ESRefreshAnimatorProtocol, ESRefreshImpactProtocol {

    public var viewContent: UIView!

    open var pullToRefreshDescription = NSLocalizedString("Pull to refresh", comment: "") {
        didSet {
            if pullToRefreshDescription != oldValue {
                titleLabel.text = pullToRefreshDescription
            }
        }
    }
    open var releaseToRefreshDescription = NSLocalizedString("Release to refresh", comment: "")
    open var loadingDescription = NSLocalizedString("Loading...", comment: "")

    open var view: UIView { return self }
    open var insets: UIEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    open var trigger: CGFloat = 200.0
    open var executeIncremental: CGFloat = 200.0
    open var state: ESRefreshViewState = .pullToRefresh

    @IBOutlet weak var backgroungImageView: UIImageView!
    @IBOutlet weak var centerImageView: UIImageView!

    fileprivate let titleLabel: UILabel = {
        let label = UILabel.init(frame: CGRect.zero)
        label.font = UIFont.systemFont(ofSize: 14.0)
        label.textColor = UIColor.init(white: 0.625, alpha: 1.0)
        label.textAlignment = .left
        return label
    }()

    fileprivate let indicatorView: UIActivityIndicatorView = {
        let indicatorView = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.large)
        indicatorView.isHidden = true
        return indicatorView
    }()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        configView()
    }

    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        xibSetup()
        configView()
    }

    func configView() {
        titleLabel.text = pullToRefreshDescription
        self.addSubview(titleLabel)
        self.addSubview(indicatorView)
    }

    open func refreshAnimationBegin(view: ESRefreshComponent) {
        print(#function)
        indicatorView.startAnimating()
        indicatorView.isHidden = false
        titleLabel.text = loadingDescription
    }

    open func refreshAnimationEnd(view: ESRefreshComponent) {
        print(#function)
        indicatorView.stopAnimating()
        indicatorView.isHidden = true
        titleLabel.text = pullToRefreshDescription
        backgroungImageView.transform = CGAffineTransform.identity
    }

    open func refresh(view: ESRefreshComponent, progressDidChange progress: CGFloat) {
        // Do nothing
        print(progress)
        let scale = max(progress, 1.0)
        self.backgroungImageView.transform = CGAffineTransform(scaleX: scale, y: scale)
    }

    open func refresh(view: ESRefreshComponent, stateDidChange state: ESRefreshViewState) {
        print(#function)
        guard self.state != state else {
            return
        }
        self.state = state

        switch state {
        case .refreshing, .autoRefreshing:
            titleLabel.text = loadingDescription
            self.setNeedsLayout()

        case .releaseToRefresh:
            titleLabel.text = releaseToRefreshDescription
            self.setNeedsLayout()
            self.impact()
            UIView.animate(withDuration: 0.3) {[weak self] in
                self?.backgroungImageView.transform = CGAffineTransform.identity
            }
        case .pullToRefresh:
            titleLabel.text = pullToRefreshDescription
            self.setNeedsLayout()
            UIView.animate(withDuration: 0.3) {[weak self] in
                self?.backgroungImageView.transform = CGAffineTransform.identity
            }

        default:
            break
        }
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        let size = self.bounds.size
        UIView.performWithoutAnimation {
            titleLabel.sizeToFit()
            titleLabel.center = CGPoint.init(x: size.width / 2.0, y: size.height / 2.0)
            indicatorView.center = CGPoint.init(x: titleLabel.frame.origin.x - 16.0, y: size.height / 2.0)
        }
    }

}
