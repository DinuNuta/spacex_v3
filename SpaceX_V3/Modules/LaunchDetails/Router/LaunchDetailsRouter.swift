//
//  LaunchDetailsRouter.swift
//  SpaceX_V3
//
//  Created by Coscodan Dinu on 10/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

protocol LaunchDetailsRouter {
    func openOnWeb(url: URL)
}

class LaunchDetailsRouterImp: LaunchDetailsRouter {

    weak var viewController: LaunchDetailsViewController!

    func openOnWeb(url: URL) {
        viewController.webOpen(url)
    }

}
