//
//  LaunchDetailsBuilder.swift
//  SpaceX_V3
//
//  Created by Coscodan Dinu on 10/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import UIKit
import Swinject

protocol LaunchDetailsBuilder {
    func buildLaunchDetailsViewController(with: Launch) -> LaunchDetailsViewController!
}

class LaunchDetailsDefaultBuilder: LaunchDetailsBuilder {

    let container = Container()

    func buildLaunchDetailsViewController(with launch: Launch) -> LaunchDetailsViewController! {

        container.register(LaunchDetailsInteractor.self) { _ in
            LaunchDetailsInteractorImp()
        }

        container.register(LaunchDetailsViewController.self) { _ in

            LaunchDetailsViewController(nibName: String(describing: LaunchDetailsViewController.self), bundle: Bundle.main)

        }.initCompleted { cont, vc in
            vc.presenter = cont.resolve(LaunchDetailsPresenter.self)
        }

        container.register(LaunchDetailsRouter.self) { cont in
            let router = LaunchDetailsRouterImp()
            router.viewController = cont.resolve(LaunchDetailsViewController.self)!
            return router
        }

        container.register(LaunchDetailsPresenter.self) { cont in
            LaunchDetailsPresenterImp(view: cont.resolve(LaunchDetailsViewController.self)!,
                                      interactor: cont.resolve(LaunchDetailsInteractor.self)!,
                                      router: cont.resolve(LaunchDetailsRouter.self)!,
                                      launch: launch)
        }

        return container.resolve(LaunchDetailsViewController.self)!
    }

    deinit {
        container.removeAll()
    }
}
