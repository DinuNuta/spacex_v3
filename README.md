# SpaceX

### SpaceX Test App technical tasks:

- App contains 2 screens (Main Screen and Launch Details Screen)
- Main Screen: Show all past launches of SpaceX
- Launch Details Screen: When user tap on a launch, show launch details
- Thumbnail images are not from API. You must use Youtube video thumbnails
- On Launch Details Screen, when user tap on image, Youtube video must be 
- [API Link](https://docs.spacexdata.com/?version=latest)
- [SpaceX API Github](https://github.com/r-spacex/SpaceX-API/wiki)

### Generamba

To create new module all you need is run in the terminal - `generamba gen NameOfYourModule swifty_viper`
And all classes and dependencies will be created for you in `Modules/` folder


### Swiftlint

* [Swiftlint](https://github.com/realm/SwiftLint)
```brew install swiftlint```
To autocorrect the code, run:
```swiftlint autocorrect --format```
To get all rules, run:
```swiftlint rules```
