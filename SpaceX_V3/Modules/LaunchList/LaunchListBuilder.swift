//
//  LaunchListBuilder.swift
//  SpaceX_V3
//
//  Created by Coscodan Dinu on 07/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import UIKit
import Swinject

protocol LaunchListBuilder {
    func buildLaunchListViewController() -> LaunchListViewController!
}

class LaunchListDefaultBuilder: LaunchListBuilder {

    let container = Container()

    func buildLaunchListViewController() -> LaunchListViewController! {

        container.register(LaunchListInteractor.self) { _ in
            LaunchListInteractorImp(launchFetcher: LaunchesServicesImp()) //Add service
        }

        container.register(LaunchListViewController.self) { _ in

            LaunchListViewController(nibName: String(describing: LaunchListViewController.self), bundle: Bundle.main)

        }.initCompleted { cont, vc in

            vc.presenter = cont.resolve(LaunchListPresenter.self)
        }

        container.register(LaunchListRouter.self) { cont in
            let router = LaunchListRouterImp()
            router.viewController = cont.resolve(LaunchListViewController.self)!
            return router
        }

        container.register(LaunchListPresenter.self) { cont in
            LaunchListPresenterImp(view: cont.resolve(LaunchListViewController.self)!,
                                   interactor: cont.resolve(LaunchListInteractor.self)!,
                                   router: cont.resolve(LaunchListRouter.self)!)
        }

        return container.resolve(LaunchListViewController.self)!
    }

    deinit {
        container.removeAll()
    }
}
