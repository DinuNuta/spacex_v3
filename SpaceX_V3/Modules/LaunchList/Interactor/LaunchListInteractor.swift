//
//  LaunchListInteractor.swift
//  SpaceX_V3
//
//  Created by Coscodan Dinu on 07/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

protocol LaunchListInteractor {
    func getLaunches(by offset: Int, completion: @escaping (Result<[Launch]>) -> Void)
}

class LaunchListInteractorImp: LaunchListInteractor {
    var launchFetcher: LaunchesServices!
    let ordered: Ordered

    init(launchFetcher: LaunchesServices, order: Ordered = .desc) {
        self.launchFetcher = launchFetcher
        self.ordered = order
    }

    func getLaunches(by offset: Int = 0, completion: @escaping (Result<[Launch]>) -> Void) {
        self.launchFetcher.getLaunches(by: offset, ordered: ordered, completion: completion)
    }

}
