//
//  LaunchTest.swift
//  SpaceX_V3Tests
//
//  Created by Dinu_c on 2/13/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import XCTest
@testable import SpaceX_V3

class LaunchTest: XCTestCase {

    private var dataSinglLaunch: Data {
        let testBundle = Bundle(for: LaunchTest.self)
        let path = testBundle.path(forResource: "FakeSingleLaunchDataJson", ofType: "json")!

        do {
            return try Data.init(contentsOf: URL.init(fileURLWithPath: path))
        } catch {
            XCTFail(error.localizedDescription)
            return Data()
        }
    }

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func assertThrowsKeyNotFound<T: Decodable>(_ expectedKey: String, decoding: T.Type, from data: Data, file: StaticString = #file, line: UInt = #line) {
        XCTAssertThrowsError(try newJSONDecoder().decode(decoding, from: data), file: file, line: line) { error in
            if case .keyNotFound(let key, _)? = error as? DecodingError {
                XCTAssertEqual(expectedKey, key.stringValue, "Expected missing key '\(key.stringValue)' to equal '\(expectedKey)'.", file: file, line: line)
            } else {
                XCTFail("Expected '.keyNotFound(\(expectedKey))' but got \(error)", file: file, line: line)
            }
        }
    }

    func testDecoding_whenMissingLinks_itThrows() throws {
        let data_ = try dataSinglLaunch.json(deletingKeyPaths: "links")
        assertThrowsKeyNotFound("links", decoding: Launch.self, from: data_)
    }

    func testDecoding_whenMissingRocket_itThrows() throws {
        let data_ = try dataSinglLaunch.json(deletingKeyPaths: "rocket")
        assertThrowsKeyNotFound("rocket", decoding: Launch.self, from: data_)
    }

    func testItShouldThrowAnErrorIfDataIsNil() {
        XCTAssertThrowsError(try Launch(data: nil), "shold throw error with description that data is nil") { (error) in
            print(error)
        }
    }

    func testShouldCreatedCorrectFieldsFromJSON() {
        let testBundle = Bundle(for: LaunchTest.self)
        let path = testBundle.path(forResource: "FakeSingleLaunchDataJson", ofType: "json")!

        do {
            let data = try Data.init(contentsOf: URL.init(fileURLWithPath: path))

            let launch = try Launch(data: data)

            XCTAssertEqual(launch.flightNumber, 17)
            XCTAssertEqual(launch.missionName, "AsiaSat 6")
            XCTAssertEqual(launch.missionId[0], "593B499")
            XCTAssertEqual(launch.launchDateUnix, Date(timeIntervalSince1970: 1410066000))

            let links = launch.links
            XCTAssertNotNil(links)
            XCTAssertEqual(links.missionPatch, "https://images2.imgbox.com/57/6a/upI6gwfq_o.png")
            XCTAssertEqual(links.missionPatchSmall, "https://images2.imgbox.com/6f/c0/D3Owbmpo_o.png")
            XCTAssertEqual(links.articleLink, "https://www.space.com/27052-spacex-launches-asiasat6-satellite.html")
            XCTAssertEqual(links.wikipedia, "https://en.wikipedia.org/wiki/AsiaSat_6")
            XCTAssertEqual(links.videoLink, "https://www.youtube.com/watch?v=39ninsyTRk8")

            XCTAssertEqual(links.flickrImages[0], "https://farm8.staticflickr.com/7604/16169087563_0e3559ab5b_o.jpg")
            XCTAssertEqual(links.flickrImages[1], "https://farm9.staticflickr.com/8742/16233828644_96738200b2_o.jpg")
            XCTAssertEqual(links.flickrImages[2], "https://farm8.staticflickr.com/7645/16601443698_e70315d1ed_o.jpg")
            XCTAssertEqual(links.flickrImages[3], "https://farm9.staticflickr.com/8730/16830335046_5f017c17be_o.jpg")
            XCTAssertEqual(links.flickrImages[4], "https://farm9.staticflickr.com/8637/16855040322_57671ab8eb_o.jpg")

            XCTAssertNil(launch.details)
            XCTAssertFalse(launch.upcoming)

            XCTAssertEqual(launch.rocket.rocketId, "falcon9")
            XCTAssertEqual(launch.rocket.rocketName, "Falcon 9")
            XCTAssertEqual(launch.rocket.rocketType, "v1.1")

            let payload = launch.rocket.secondStage.payloads[0]
            XCTAssertEqual(launch.rocket.secondStage.block, 1)
            XCTAssertEqual(payload.payloadId, "AsiaSat 6")
            XCTAssertEqual(payload.noradId[0], 40141)
            XCTAssertFalse(payload.reused)
            XCTAssertEqual(payload.customers[0], "AsiaSat")
            XCTAssertEqual(payload.nationality, "Hong Kong")
            XCTAssertEqual(payload.manufacturer, "SSL")
            XCTAssertEqual(payload.payloadType, "Satellite")
            XCTAssertEqual(payload.payloadMassKg, 4428)
            XCTAssertEqual(payload.payloadMassLbs, 9762)
            XCTAssertEqual(payload.orbit, "GTO")

        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func testItShouldThrowAnErrorIfDataIsInvalid() {
        let data = "{\"invalidData\" : 0}".data(using: .utf8)
        XCTAssertThrowsError(try Launch(data: data), "shold throw a decoding error") { (error) in
            print(error)
        }
    }

    //    func testPerformanceExample() {
    //        // This is an example of a performance test case.
    //        self.measure {
    //            // Put the code you want to measure the time of here.
    //        }
    //    }

}

extension Data {
    func json(updatingKeyPaths keyPaths: (String, Any)...) throws -> Data {
        let decoded = try JSONSerialization.jsonObject(with: self, options: .mutableContainers) as AnyObject

        for (keyPath, value) in keyPaths {
            decoded.setValue(value, forKeyPath: keyPath)
        }

        return try JSONSerialization.data(withJSONObject: decoded)
    }

    func json(deletingKeyPaths keyPaths: String...) throws -> Data {
        let decoded = try JSONSerialization.jsonObject(with: self, options: .mutableContainers) as AnyObject

        for keyPath in keyPaths {
            decoded.setValue(nil, forKeyPath: keyPath)
        }

        return try JSONSerialization.data(withJSONObject: decoded)
    }
}
