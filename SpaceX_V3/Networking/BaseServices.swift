//
//  BaseServices.swift
//  SpaceX_V3
//
//  Created by Dinu_c on 2/6/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import Alamofire

struct RequestHttp {

    let method: Alamofire.HTTPMethod
    let url: URL
    let parameters: [String: Any]?
    let encoding: ParameterEncoding
    let headers: [String: String]?
    let data: Data?

    init(url: URL,
         method: Alamofire.HTTPMethod = .get,
         parameters: [String: Any]? = nil,
         encoding: ParameterEncoding = URLEncoding.default,
         headers: [String: String]? = nil,
         data: Data? = nil) {

        self.url = url
        self.method = method
        self.parameters = parameters
        self.encoding = encoding
        self.headers = headers
        self.data = data
    }
}

struct RequestMultipartData {

    let method: Alamofire.HTTPMethod
    let url: URL
    let parameters: [String: Any]?
    let encoding: ParameterEncoding
    let headers: [String: String]?
    let data: Data?
    let filesPath: [String]?
    let mimeType: FileMimeType

    init(url: URL, method: Alamofire.HTTPMethod = .get,
         parameters: [String: Any]? = nil,
         headers: [String: String]? = nil,
         encoding: ParameterEncoding = JSONEncoding.default,
         data: Data? = nil,
         filesPath: [String]?=nil,
         mimeType: FileMimeType = .image) {

        self.url = url
        self.method = method
        self.parameters = parameters
        self.encoding = encoding
        self.headers = headers
        self.data = data
        self.filesPath = filesPath
        self.mimeType = mimeType
    }
}

enum FileMimeType: String {
    case image = "image/jpg"
    case video = "video/mp4"
    case audio = "audio/m4a"
    case pdf = "application/pdf"
}

protocol BaseServices {
    /// Alamofire SessionManager has default implementation autorised and default headers
    var requestManager: SessionManager {get set}
    var isReachable: Bool { get }
    @discardableResult func requestTask<T: Codable>(_ request: RequestHttp, completion: @escaping (Result<[T]>) -> Void) -> URLSessionTask?
}

extension BaseServices {
    /// Authorised Alamofire SessionManager with default headers
    //    var requestManager: SessionManager {
    //        get {return Authorised.sender}
    //        set {}
    //    }

    var reachability: NetworkReachabilityManager? {
        NetworkReachabilityManager()
    }

    var isReachable: Bool { return reachability?.isReachable ?? false }

    @discardableResult func requestTask<T: Codable>(_ request: RequestHttp, completion: @escaping (Result<[T]>) -> Void) -> URLSessionTask? {
        guard isReachable else {
            completion(.failure(ErrorMessages.noInternetConnection(message: reachability?.networkReachabilityStatusDescription)))
            return nil
        }
        return requestManager.request(request.url, method: request.method, parameters: request.parameters, encoding: request.encoding)
            .validateErrors()
            .responseDecodable(queue: nil, keyPath: nil, completionHandler: { (response) in
                completion(response.result)
            }).task
    }

    func request<T: Codable>(_ request: RequestHttp, completion: @escaping (Result<[T]>) -> Void) {
        guard isReachable else {
            completion(.failure(ErrorMessages.noInternetConnection(message: reachability?.networkReachabilityStatusDescription)))
            return
        }
        requestManager.request(request.url, method: request.method, parameters: request.parameters, encoding: request.encoding)
            .validateErrors()
            .responseDecodable(queue: nil, keyPath: nil, completionHandler: { (response) in
                completion(response.result)
            })
    }

    func request<T: Codable>(_ request: RequestHttp, completion: @escaping (Result<T>) -> Void) {
        guard isReachable else {
            completion(.failure(ErrorMessages.noInternetConnection(message: reachability?.networkReachabilityStatusDescription)))
            return
        }
        requestManager.request(request.url, method: request.method, parameters: request.parameters, encoding: request.encoding)
            .validateErrors()
            .responseDecodable(queue: nil, keyPath: nil, completionHandler: { (response) in
                completion(response.result)
            })
    }

    func request<T: Codable>(_ request: RequestHttp, completion: @escaping (Result<T?>) -> Void) {
        guard isReachable else {
            completion(.failure(ErrorMessages.noInternetConnection(message: reachability?.networkReachabilityStatusDescription)))
            return
        }
        requestManager.request(request.url, method: request.method, parameters: request.parameters, encoding: request.encoding)
            .validateErrors()
            .responseDecodable(queue: nil, keyPath: nil, completionHandler: { (response) in
                completion(response.result)
            })
    }

    func upload<T: Codable>(request: RequestMultipartData, progressHandler: ((Double) -> Void)?, completion: @escaping (Result<T?>) -> Void) {
        requestManager.upload(multipartFormData: { multipartFormData in
            if let files = request.filesPath?.compactMap({URL(string: $0)}) {
                files.forEach { url in
                    let uuid = UUID().uuidString
                    multipartFormData.append(url, withName: constants.networking_file, fileName: uuid, mimeType: request.mimeType.rawValue)
                }
            }
            if let data = request.data {
                let uuid = UUID().uuidString
                multipartFormData.append(data, withName: constants.networking_file, fileName: uuid, mimeType: request.mimeType.rawValue)
            }
            request.parameters?.forEach({
                guard let valueString = $0.value as? String else { return }
                guard let valueData = valueString.data(using: .utf8) else { return }
                multipartFormData.append(valueData, withName: $0.key)})
        }, to: request.url, method: .patch, headers: request.headers,
           encodingCompletion: { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress { (progress) in
                    let progres = progress.fractionCompleted * 100
                    progressHandler?(progres)
                }
                upload.responseDecodable(queue: nil, keyPath: nil, completionHandler: { (response) in
                    completion(response.result)
                })
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    func uploadData<T: Codable>(request: RequestMultipartData, progressHandler: ((Double) -> Void)?, completion: @escaping (Result<T?>) -> Void) {
        let uuid = UUID().uuidString
        guard let data = request.data else {return}

        requestManager.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(data, withName: constants.networking_file, fileName: uuid, mimeType: request.mimeType.rawValue)
            request.parameters?.forEach({
                guard let valueString = $0.value as? String else { return }
                guard let valueData = valueString.data(using: .utf8) else { return }
                multipartFormData.append(valueData, withName: $0.key)})
        }, to: request.url, method: .patch, headers: request.headers,
           encodingCompletion: { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress { progress in
                    let progres = progress.fractionCompleted * 100
                    progressHandler?(progres)
                }
                upload.responseDecodable(queue: nil, keyPath: nil, completionHandler: { (response) in
                    completion(response.result)
                })
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}
