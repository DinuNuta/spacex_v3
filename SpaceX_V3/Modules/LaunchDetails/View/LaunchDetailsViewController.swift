//
//  LaunchDetailsViewController.swift
//  SpaceX_V3
//
//  Created by Coscodan Dinu on 10/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

protocol LaunchDetailsView: class {
    func config(with viewModel: LaunchDetailsViewModel)
}

class LaunchDetailsViewController: UIViewController, LaunchDetailsView {

    var presenter: LaunchDetailsPresenter!

    @IBOutlet weak var launcheDateLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var rocketNameLabel: UILabel!
    @IBOutlet weak var payloadmassLabel: UILabel!
    @IBOutlet weak var playerView: YTPlayerView!
    @IBOutlet weak var wikipediaButton: UIButton!

    var wikipediaLink: URL? {
        didSet {
            self.wikipediaButton.isHidden = wikipediaLink == nil
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let viewModel = presenter.getViewModel() else {return}
        config(with: viewModel)
    }

    @IBAction func tapOnWikipedia(_ sender: Any) {
        presenter.openWekipedia()
    }
}

extension LaunchDetailsViewController {
    func config(with viewModel: LaunchDetailsViewModel) {
        wikipediaLink = URL.init(string: viewModel.wikipediaLink ?? "https://en.wikipedia.org/wiki")

        if let videoId = viewModel.videoID {
            self.playerView.load(withVideoId: videoId)
        }

        DispatchQueue.main.async {
            self.title = viewModel.title
            self.launcheDateLabel.text = viewModel.launcheDate
            self.rocketNameLabel.text = "Rocket name: " + (viewModel.rocketName ?? "-")
            self.descriptionTextView.text = viewModel.description
            self.payloadmassLabel.text = "Payload mass: " + (viewModel.mass ?? "-")
        }
    }
}

extension LaunchDetailsViewController: OpeningURLInBrowser {}
