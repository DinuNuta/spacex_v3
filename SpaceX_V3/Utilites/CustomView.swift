import Foundation
import UIKit

// MARK: - CustomView

public protocol CustomView: NibLoadableView {
    var viewContent: UIView! {get set}
}

public extension CustomView where Self: UIView {
    func xibSetup() {
        viewContent = loadViewFromNib()
        viewContent.frame = bounds
        viewContent.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]

        addSubview(viewContent)
    }
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: Self.NibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
