//
//  GenericServices.swift
//  SpaceX_V3
//
//  Created by Dinu_c on 2/6/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import Alamofire

/// API service Get List of entity with pagination
protocol EntityGetter_protocol: class {
    associatedtype EntityType: Codable

    var url: URL {get set}
    var next: URL? {get set}

    func getEntity(completion: @escaping (Result<[EntityType]>) -> Void)
    func getNextPage(completion: @escaping (Result<[EntityType]>) -> Void)
    func getEntyties(url: URL, completion: @escaping (Result<[EntityType]>) -> Void)
}

extension EntityGetter_protocol {
    func getEntity(completion: @escaping (Result<[EntityType]>) -> Void) {
        getEntyties(url: url, completion: completion)
    }

    func getNextPage(completion: @escaping (Result<[EntityType]>) -> Void) {
        if let next = next {
            getEntyties(url: next, completion: completion)
        } else {
            completion(.success([]))
        }
    }

    func getEntyties(url: URL, completion: @escaping (Result<[EntityType]>) -> Void) {
        Authorised.sender.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .validateErrors()
            .responseData {[weak self] (response) in
                do {
                    let data = try response.result.unwrap()
                    let pagination = try Pagination<EntityType>.init(data: data, keyPath: nil)
                    let entities = pagination.results
                    self?.next = pagination.next.flatMap({URL(string: $0)})
                    completion(.success(entities))
                } catch {
                    completion(.failure(error))
                }
        }
    }
}

// MARK: - ajusting to backend API
class EntityServices<T: Codable>: EntityGetter_protocol {
    typealias EntityType = T

    var next: URL?
    var prev: URL?
    var url: URL

    init(vault url: URL) {
        self.url = url
    }
}

//for example extension
//extension EntityServices where T == Product{
//
//    func openedProduct(id: Int, completion: @escaping (Result<Message>)->()){
//        Authorised.sender.request(URLs.opened(product: id))
//            .validateErrors()
//            .responseMessage { (response) in
//                completion(response.result)
//        }
//    }
//}
