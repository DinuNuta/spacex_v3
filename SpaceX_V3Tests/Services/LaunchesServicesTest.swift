//
//  LaunchesServicesTest.swift
//  SpaceX_V3Tests
//
//  Created by Dinu_c on 2/13/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import XCTest
@testable import SpaceX_V3
@testable import Alamofire

class LaunchesServicesTest: XCTestCase {

    var sut: LaunchesServicesImp!

    override func setUp() {
        super.setUp()

        let manager: SessionManager = {
            let configuration: URLSessionConfiguration = {
                let configuration = URLSessionConfiguration.default
                configuration.protocolClasses = [MockURLProtocol.self]
                return configuration
            }()

            return SessionManager(configuration: configuration)
        }()
        sut = LaunchesServicesImp(requestManager: manager)
    }

    override func tearDown() {
        sut = nil
    }

    private var dataLaunchList: Data {
        let testBundle = Bundle(for: LaunchTest.self)
        let path = testBundle.path(forResource: "FakeLaunchDataJSON", ofType: "json")!

        do {
            return try Data.init(contentsOf: URL.init(fileURLWithPath: path))
        } catch {
            XCTFail(error.localizedDescription)
            return Data()
        }
    }

    func testReturnLaunches() {
        MockURLProtocol.response(url: URLs.pastLaunches, data: dataLaunchList, status: 200)

        let expectation = XCTestExpectation(description: "Performs a request")

        sut.getLaunches(by: 0, ordered: .desc) { (result) in
            do {
                let launchs = try result.get()
                XCTAssertFalse(launchs.isEmpty)
                expectation.fulfill()
            } catch {
                XCTFail(error.localizedDescription)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 3)
    }

    //    func testPerformanceExample() {
    //        // This is an example of a performance test case.
    //        self.measure {
    //            // Put the code you want to measure the time of here.
    //        }
    //    }

}
