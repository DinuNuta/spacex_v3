//
//  LaunchDetailsViewModel.swift
//  SpaceX_V3
//
//  Created by Dinu_c on 2/10/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

struct LaunchDetailsViewModel {
    let title: String
    let description: String?
    let launcheDate: String?
    let rocketName: String?
    let mass: String?
    let videoID: String?
    let wikipediaLink: String?
}
