//
//  LaunchDetailsPresenter.swift
//  SpaceX_V3
//
//  Created by Coscodan Dinu on 10/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

protocol LaunchDetailsPresenter {
    func getViewModel() -> LaunchDetailsViewModel?
    func openWekipedia()
}

class LaunchDetailsPresenterImp: LaunchDetailsPresenter {

    weak var view: LaunchDetailsView?
    var interactor: LaunchDetailsInteractor!
    var router: LaunchDetailsRouter!

    var launch: Launch?

    init(view: LaunchDetailsView, interactor: LaunchDetailsInteractor, router: LaunchDetailsRouter, launch: Launch?) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.launch = launch
    }

    func getViewModel() -> LaunchDetailsViewModel? {
        guard let launch = self.launch else {return nil}
        let viewModel = LaunchDetailsViewModel.init(title: launch.missionName,
                                                    description: launch.details,
                                                    launcheDate: launch.launchDateUnix?.iso8601Full,
                                                    rocketName: launch.rocket.rocketName,
                                                    mass: String("\(launch.massKG) kg"),
                                                    videoID: launch.links.youtubeId,
                                                    wikipediaLink: launch.links.wikipedia)
        return viewModel
    }

    func openWekipedia() {
        guard let wikipedia = launch?.links.wikipedia,
            let wikipediaLink = URL(string: wikipedia) else { return }

        router.openOnWeb(url: wikipediaLink)
    }
}
